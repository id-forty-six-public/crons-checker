<?php

namespace idfortysix\cronschecker;

use Illuminate\Console\Command;

class CheckCrons extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checkcrons';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cron checker';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = "/etc/cron.d/";
        $cronfile = file_get_contents($path."app");
        preg_match_all("/[a-z0-9\/_\.\-]{1,}\.log/ui", $cronfile, $matches);

        $bad = array();

        foreach ($matches[0] as $logfiletemp) {

            $logfile = trim($logfiletemp);

            if ($logfile != '') {

                // ++  funkcija tikrinanti ar paksutinis logo timestampas nerqa mazesnis nei dabartinis timestamp
    
                if (file_exists($logfile)) {
                    
                    $atsakymas = exec ("tail -n 1000 ".$logfile." | grep SECONDCHECK | tail -1");

                    // tikrinimas ar time stamp 
                    $kintamieji = explode(":", trim($atsakymas));
                    
                    if (isset($kintamieji[1])) {

                        $secondcheck = intval($kintamieji[1]);

                        if (time() > $secondcheck) {
                        
                            $bad[] = str_replace($path."logs/", "", $logfile);

                        }

                    } else {

                        if (file_exists($logfile.".1.gz")) {

                            $atsakymas = exec ("zcat ".$logfile.".1.gz | grep SECONDCHECK | tail -1");

                            // tikrinimas ar time stamp 
                            $kintamieji = explode(":", trim($atsakymas));
                            
                            if (isset($kintamieji[1])) {

                                $secondcheck = intval($kintamieji[1]);

                                if (time() > $secondcheck) {
                                
                                    $bad[] = str_replace($path."logs/", "", $logfile);

                                }

                            } else {

                                $bad[] = str_replace($path."logs/", "", $logfile);

                            }
                        
                        } else {

                            $bad[] = str_replace($path."logs/", "", $logfile);

                        }

                    }

                } else {

                    $bad[] = str_replace($path."logs/", "", $logfile);
                    continue;

                }

            }

        }

        $count = count($bad);

        if ($count)
		{
            foreach ($bad as $key => $value) {
                $bad[$key] = str_replace([".log", "_"], ["", "/"], $value);
            }

            $this->error("CRITICAL - ".$count." (".implode(", ", $bad).")");
        }
		else
		{
            $this->info("OK - All crons run successfully");
        }

    }
}
