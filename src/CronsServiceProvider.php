<?php

namespace idfortysix\cronschecker;

use Illuminate\Support\ServiceProvider;

class CronsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                CheckCrons::class,
            ]);
    	}
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        
    }
}

